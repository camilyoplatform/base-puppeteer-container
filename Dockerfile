# image name: camilyo/base-puppeteer-container
FROM node:10.16

# install missing packages for Puppeteer and clean apt lists
# (https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md)
RUN apt-get update \
    && apt-get install -qq --no-install-recommends gconf-service libasound2 libatk-bridge2.0-0 libgconf-2-4 libgtk-3-0 libnspr4 libx11-xcb1 libxss1 libxtst6 libappindicator1 libnss3 lsb-release \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*