# Base Puppeteer Container

A base image with `apt` packages required to run [Puppeteer](https://github.com/GoogleChrome/puppeteer) (and are missing from base node image)

## Build

```powershell
.\build.ps1
```

## DockerHub Integration

DockerHub is connected to this repository, and will automatically start building the image for every commit to master.

## Manually Push to DockerHub

Login to DockerHub and make sure you have access to Camilyo organization. Then Run:

```powershell
.\push.ps1
```

## Meta

- [Base image on DockerHub](https://cloud.docker.com/u/camilyo/repository/docker/camilyo/base-snapshot-service-image)
- [Puppeteer troubleshooting](https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md)
